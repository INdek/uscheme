use std::collections::HashMap;


#[derive(Debug, Clone)]
pub struct Args {
  pub program_name: Option<String>,
  pub args: HashMap<String, String>,
  pub filenames: Vec<String>,
}

impl Args {
  pub fn get<N: AsRef<str>>(&self, name: N) -> Option<String> {
    let name = name.as_ref();

    self.args.get(name)
      .map(Clone::clone)
  }
}

#[derive(Debug, Clone)]
pub struct ArgParse {
  args: HashMap<String, (Option<char>, Option<String>)>,
  parse_filenames: bool,
}

impl ArgParse {
  pub fn new() -> Self {
    ArgParse {
      args: HashMap::new(),
      parse_filenames: false,
    }
  }

  pub fn filenames(mut self) -> Self {
    self.parse_filenames = true;
    self
  }

  pub fn arg<N: Into<String>>(mut self, arg_name: N, short: Option<char>, long: Option<&str>) -> Self {
    let arg_name = arg_name.into();
    let long = long.map(String::from);

    self.args.insert(arg_name, (short, long));

    self
  }

  fn insert_flag(&self, flag: String, arg_value: String, args: &mut HashMap<String, String>) {
    self.args.iter()
      .find(move |(_, (short, long))| {
        let flag = Some(flag.clone());
        let short = short.map(|c| c.to_string());
        let long = long.clone();

        flag == short || flag == long
      })
      .map(|(key, _)| key.to_string())
      .map(|key| args.insert(key, arg_value));
  }

  fn parse_flag(&self, arg_string: String, args: &mut HashMap<String, String>) {
    let is_long = arg_string.starts_with("--");
    let arg_string = arg_string.trim_start_matches('-');

    let mut aiter = arg_string.splitn(2, '=');
    let flags = aiter.next()
      .map(String::from)
      .unwrap();

    let arg_value = aiter.next()
      .map(String::from)
      .unwrap_or_else(|| String::new());

    if is_long {
      self.insert_flag(flags, arg_value, args);
    } else {
      let flag_count = flags.len();
      flags
        .chars()
        .map(|c| c.to_string())
        .enumerate()
        .map(|(i, c)| if flag_count == i + 1 { (c, arg_value.clone()) } else { (c, String::new()) })
        .for_each(|(c, arg)| self.insert_flag(c, arg, args));
    }
  }

  pub fn parse<T: AsRef<str>>(&mut self, args: T) -> Args {
    let mut args_iter = args.as_ref()
      .split_whitespace()
      .map(String::from);

    let program_name = args_iter.next();
    let mut args_hm = HashMap::new();
    let mut filenames = vec![];

    for arg_entry in args_iter {
      let is_flag = arg_entry.starts_with('-');
      if is_flag {
        self.parse_flag(arg_entry, &mut args_hm);
      } else if self.parse_filenames {
        // If its not a flag, it must be a filename
        filenames.push(arg_entry);
      }
    }

    Args {
      program_name,
      args: args_hm,
      filenames,
    }
  }
}


#[cfg(test)]
mod tests {
  use super::*;

  mod program_name {
    use super::*;
    #[test]
    fn parse_program_name_empty() {
      let args = ArgParse::new().parse("");
      assert_eq!(args.program_name, None);
    }

    #[test]
    fn parse_program_name() {
      let args = ArgParse::new().parse("program.exe");
      assert_eq!(args.program_name, Some("program.exe".into()));
    }

    #[test]
    fn parse_program_name_path() {
      let args = ArgParse::new().parse("/asd/sda/program");
      assert_eq!(args.program_name, Some("/asd/sda/program".into()));
    }

    #[test]
    fn parse_program_name_path_windows() {
      let args = ArgParse::new().parse(r"folder\das\asdprogram.exe");
      assert_eq!(args.program_name, Some(r"folder\das\asdprogram.exe".into()));
    }
  }

  mod arg {
    use super::*;

    #[test]
    fn parse_long_form_none() {
      let args = ArgParse::new()
        .arg("foo", None, Some("foo"))
        .parse("");
      assert_eq!(args.get("foo"), None);
    }

    #[test]
    fn parse_long_form_no_value() {
      let args = ArgParse::new()
        .arg("foo", None, Some("foo"))
        .parse("program.exe --foo");
      assert_eq!(args.get("foo"), Some("".into()));
    }

    #[test]
    fn parse_long_form_eq_value() {
      let args = ArgParse::new()
        .arg("foo", None, Some("foo"))
        .parse("program.exe --foo=asd_asd");
      assert_eq!(args.get("foo"), Some("asd_asd".into()));
    }

    #[test]
    #[ignore]
    fn parse_long_form_space_value() {
      let args = ArgParse::new()
        .arg("foo", None, Some("foo"))
        .parse("program.exe --foo asd_asd");
      assert_eq!(args.get("foo"), Some("asd_asd".into()));
    }

    #[test]
    fn parse_short_form_no_value() {
      let args = ArgParse::new()
        .arg("foo", Some('f'), None)
        .parse("");
      assert_eq!(args.get("foo"), None);
    }

    #[test]
    fn parse_short_form_flag() {
      let args = ArgParse::new()
        .arg("foo", Some('f'), None)
        .parse("program -f");
      assert_eq!(args.get("foo"), Some("".into()));
    }

    #[test]
    fn parse_short_form_eq_value() {
      let args = ArgParse::new()
        .arg("foo", Some('f'), None)
        .parse("program -f=sad");
      assert_eq!(args.get("foo"), Some("sad".into()));
    }

    #[test]
    #[ignore]
    fn parse_short_form_space_value() {
      let args = ArgParse::new()
        .arg("foo", Some('f'), None)
        .parse("program -f sad");
      assert_eq!(args.get("foo"), Some("sad".into()));
    }



    #[test]
    fn parse_short_form_multi_flag() {
      let args = ArgParse::new()
        .arg("foo", Some('f'), None)
        .arg("bar", Some('g'), None)
        .parse("program -gf");

      assert_eq!(args.get("foo"), Some("".into()));
      assert_eq!(args.get("bar"), Some("".into()));
    }

    #[test]
    fn parse_short_form_multi_flag_value() {
      let args = ArgParse::new()
        .arg("foo", Some('f'), None)
        .arg("bar", Some('g'), None)
        .parse("program -gf=asd");

      assert_eq!(args.get("foo"), Some("asd".into()));
      assert_eq!(args.get("bar"), Some("".into()));
    }
  }

  mod filename {
    use super::*;


    #[test]
    fn parse_filename_nostring() {
      let args = ArgParse::new()
        .filenames()
        .parse("");

      let target: Vec<String> = vec![];
      assert_eq!(args.filenames, target);
    }

    #[test]
    fn parse_filename_none() {
      let args = ArgParse::new()
        .filenames()
        .parse("program.exe");

      let target: Vec<String> = vec![];
      assert_eq!(args.filenames, target);
    }

    #[test]
    fn parse_filename() {
      let args = ArgParse::new()
        .filenames()
        .parse("program.exe /the/path/to/the/file.exe");

      assert_eq!(args.filenames, ["/the/path/to/the/file.exe"]);
    }

    #[test]
    fn parse_multi_filename() {
      let args = ArgParse::new()
        .filenames()
        .parse("program.exe file1 file2 file3");

      assert_eq!(args.filenames, ["file1", "file2", "file3"]);
    }
  }
}