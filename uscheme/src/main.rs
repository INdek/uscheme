extern crate uscheme_lib;

mod argparse;

use argparse::{ArgParse, Args};
use uscheme_lib::backend::Backend;
use std::{
    fs,
    env,
    io::{self, prelude::*},
};

fn print_indicator() {
    print!("> ");
    io::stdout().flush().ok().expect("Could not flush stdout");
}

fn repl(mut backend: Backend, _args: &Args) {
    let stdin = io::stdin();

    print_indicator();
    for line in stdin.lock().lines() {
        let line = line.unwrap();
        let line_bytes = line.as_bytes();
        let ast_res = uscheme_lib::parse::parse(line_bytes);

        println!("ast: {:?}", ast_res);

        if let Ok(ast) = ast_res {
            let eval_res = backend.eval_ast_list(ast.1);

            println!("eval: {}", match eval_res {
                Ok(Some(eval)) => format!("{}", eval),
                Ok(None) => format!(""),
                Err(e) => format!("Convert Error({})", e),
            });
        }


        print_indicator();
    }
}

fn eval_program(mut backend: Backend, args: &Args) {
    for path in args.filenames.clone() {
        let mut file = fs::File::open(path.clone())
            .expect(&format!("Failed to open file: {}", path));

        let mut file_contents = String::new();
        file.read_to_string(&mut file_contents)
            .expect("Failed to read file");

        let res = backend.eval_string(file_contents);
        match res {
            Ok(r) => println!("OK!: {:?}", r),
            Err(e) => println!("Error: {:?}", e),
        }
    }
}

fn print_help(args: &Args) {
    let program_name = args.program_name.clone().unwrap_or("uscheme".into());
    println!(r#"{} <filename>

    uscheme interperter
    "#, program_name);
}

fn build_backend(_args: &Args) -> Backend {
    Backend::new()
}

fn main() {
    let cmd = env::args().collect::<Vec<String>>().join(" ");
    let args = ArgParse::new()
        .filenames()
        .arg("help", Some('h'), Some("help"))
        .parse(cmd);

    if args.get("help").is_some() {
        return print_help(&args);
    }

    let backend = build_backend(&args);
    if args.filenames.len() == 0 {
        repl(backend, &args);
    } else {
        eval_program(backend, &args);
    }
}
