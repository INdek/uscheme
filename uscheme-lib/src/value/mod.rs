use alloc::string::{String, ToString};
use itertools::Itertools;
use core::{fmt, ops};
use backend::Backend;
use parse::Ast;
use atoi::atoi;
use checked::Checked;
use list::List;
use traits::*;

#[cfg(feature = "rational-numbers")]
pub(crate) mod rational;

pub type Identifier = String;

pub type BuiltinFunction = fn(&mut Backend, List<Value>) -> Value;

pub type Integer = i32;

pub type Boolean = bool;

#[derive(Debug, Clone, PartialEq)]
pub enum Error {
    DivByZero,
    InvalidOperation,
    ArityError,
    UnboundVariable,
    IntegerOverflow,
    IntegerUnderflow,
    ArithmeticError
}


impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::DivByZero => write!(f, "Division by zero"),
            Error::InvalidOperation => write!(f, "Invalid operation"),
            Error::ArityError => write!(f, "Invalid Arity"),
            Error::UnboundVariable => write!(f, "Unbound variable"),
            Error::IntegerOverflow => write!(f, "Integer Overflow"),
            Error::IntegerUnderflow => write!(f, "Integer Underflow"),
            Error::ArithmeticError => write!(f, "Arithmetic Error"),
        }
    }
}




#[derive(Clone)]
pub enum Value {
    String(String),
    Boolean(Boolean),

    #[cfg(feature = "rational-numbers")]
    Rational(rational::Rational),

    Integer(Integer),
    Identifier(Identifier),
    Symbol(Identifier),
    Error(Error),
    Sexp(List<Value>),
    Qexp(List<Value>),
    BuiltinFunction(&'static BuiltinFunction),
    Function {
        args: List<Value>,
        body: List<Value>,
    },
}

impl fmt::Debug for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Value::String(v) => write!(f, "(str {:?})", v),
            Value::Boolean(v) => write!(f, "(bool {:?})", v),
            Value::Integer(v) => write!(f, "(int {:?})", v),

            #[cfg(feature = "rational-numbers")]
            Value::Rational(v) => write!(f, "(rational {:?})", v),

            Value::Identifier(v) => write!(f, "(ident {:?})", v),
            Value::Symbol(v) => write!(f, "(symbol {:?})", v),
            Value::Error(v) => write!(f, "(err {:?})", v),
            Value::Sexp(v) => write!(f, "(sexp {:?})", v),
            Value::Qexp(v) => write!(f, "(qexp {:?})", v),
            Value::BuiltinFunction(_) => write!(f, "<builtin function>"),
            Value::Function { args, body } => write!(f,
                   "(lambda {:?}\n\t{})",
                   args,
                   format!("{:?}", body).replace('\n', "\n\t")
            ),
        }
    }
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Value::String(s) => write!(f, "\"{}\"", s),
            Value::Boolean(true) => write!(f, "#t"),
            Value::Boolean(false) => write!(f, "#f"),

            #[cfg(feature = "rational-numbers")]
            Value::Rational(v) => write!(f, "{}", v),

            Value::Integer(i) => write!(f, "{}", i),
            Value::Error(e) => write!(f, "Error({})", e),
            Value::Identifier(s) => write!(f, "{}", s),
            Value::Symbol(s) => write!(f, "'{}", s),
            Value::Sexp(v) => {
                write!(f, "({})", v.iter().format_with(" ", |value, f| {
                    f(&format_args!("{}", value))
                }))
            }
            Value::Qexp(v) => {
                // TODO: Maybe we can merge this with the Sexp printer?
                write!(f, "'({})", v.iter().format_with(" ", |value, f| {
                    f(&format_args!("{}", value))
                }))
            }
            Value::BuiltinFunction(_) => write!(f, "<builtin function>"),
            Value::Function { args, body } => write!(f,
                   "(lambda {}\n\t{})",
                   Value::Sexp(args.clone()),
                   format!("({})", body.iter().format_with("\n", |value, f| {
                       f(&format_args!("{}", value))
                   })).replace('\n', "\n\t")
            ),
        }
    }
}


impl PartialEq for Value {
    fn eq(self: &Value, other: &Value) -> bool {
        match (self, other) {
            (Value::String(l), Value::String(r)) => l == r,
            (Value::Boolean(l), Value::Boolean(r)) => l == r,

            #[cfg(feature = "rational-numbers")]
            (Value::Rational(l), Value::Rational(r)) => l == r,

            (Value::Integer(l), Value::Integer(r)) => l == r,
            (Value::Identifier(l), Value::Identifier(r)) => l == r,
            (Value::Symbol(l), Value::Symbol(r)) => l == r,
            (Value::Error(l), Value::Error(r)) => l == r,
            (Value::Sexp(l), Value::Sexp(r)) => l == r,
            (Value::Qexp(l), Value::Qexp(r)) => l == r,
            (Value::BuiltinFunction(l), Value::BuiltinFunction(r)) => ::core::ptr::eq(*l, *r),
            (
                Value::Function{ args: largs, body: lbody },
                Value::Function{ args: rargs, body: rbody }
            ) => largs == rargs && lbody == rbody,
            _ => false
        }
    }
}

impl Value {
    // TODO: String is temporary, we should build a proper error type

    fn parse_integer(mut istr: &str) -> Result<i32, String> {
        // TODO: There probably is some way to make this prettier
        let is_neg = istr.starts_with('-');
        if is_neg {
            istr = &istr[1..];
        }

        atoi::<Checked<Integer>>(istr.as_bytes())
            .map(|checked| match (is_neg, checked.0) {
                (true, Some(v)) => Ok(v * -1),
                (false, Some(v)) => Ok(v),
                _ => Err("Integer over capacity".into())
            })
            .unwrap_or(Err("Invalid integer".into()))
    }

    pub fn from_ast<'a>(value: Ast<'a>) -> Result<Option<Self>, String> {
        match value {
            Ast::Comment(_) => Ok(None),
            Ast::String(sstr) => Ok(Some(Value::String(sstr.to_string()))),
            Ast::Boolean(bstr) => match bstr {
                "#t" => Ok(Some(Value::Boolean(true))),
                "#f" => Ok(Some(Value::Boolean(false))),
                _ => Err(format!("Invalid boolean expression: {}", bstr)),
            }
            Ast::Rational((num, denom)) => {
                #[cfg(not(feature = "rational-numbers"))]
                {
                    Err(String::from("Parsed rational number, but `rational-numbers` feature not enabled"))
                }

                #[cfg(feature = "rational-numbers")]
                {
                    Ok({
                        let num = Value::parse_integer(num)?;
                        let denom = Value::parse_integer(denom)?;
                        let rational: Result<rational::Rational, String> = rational::Rational::checked_new(num, denom)
                            .ok_or("Invalid rational".into());

                        Some(Value::Rational(rational?))
                    })
                }
            },
            Ast::Integer(istr) => Value::parse_integer(istr)
                .map(Value::Integer)
                .map(Some),
            Ast::Identifier(ident) => Ok(Some(Value::Identifier(ident.to_string()))),
            Ast::Symbol(sym) => Ok(Some(Value::Symbol(sym.to_string()))),
            Ast::Sexp(list) => Ok(
                list.into_iter()
                    .map(Value::from_ast)
                    .collect::<Result<Option<List<Value>>, String>>()?
                    .map(Value::Sexp)
            ),
            Ast::Qexp(list) => Ok(
                list.into_iter()
                    .map(Value::from_ast)
                    .collect::<Result<Option<List<Value>>, String>>()?
                    .map(Value::Qexp)
            ),
        }
    }


    pub fn is_error(&self) -> bool {
        match self {
            Value::Error(_) => true,
            _ => false,
        }
    }
}

impl ops::Neg for Value {
    type Output = Value;

    fn neg(self) -> Value {
        match self {
            err @ Value::Error(_) => err,

            Value::Integer(i) =>
                i.checked_neg()
                    .map(Value::Integer)
                    .unwrap_or(Value::Error(Error::ArithmeticError)),

            #[cfg(feature = "rational-numbers")]
            Value::Rational(r) =>
                r.checked_neg()
                    .map(Value::Rational)
                    .unwrap_or(Value::Error(Error::ArithmeticError)),

            _ => Value::Error(Error::InvalidOperation),
        }
    }
}


impl ops::Add for Value {
    type Output = Value;

    fn add(self, other: Value) -> Value {
        match (self, other) {
            (err @ Value::Error(_), _) | (_, err @ Value::Error(_)) => err,
            (Value::Integer(il), Value::Integer(ir)) =>
                il.checked_add(ir)
                    .map(Value::Integer)
                    .unwrap_or(Value::Error(Error::IntegerOverflow)),

            #[cfg(feature = "rational-numbers")]
            (Value::Rational(il), Value::Rational(ir)) =>
                il.checked_add(ir)
                    .map(Value::Rational)
                    .unwrap_or(Value::Error(Error::IntegerOverflow)),
            #[cfg(feature = "rational-numbers")]
            (Value::Integer(il), Value::Rational(ir)) =>
                ir.checked_add(il)
                    .map(Value::Rational)
                    .unwrap_or(Value::Error(Error::IntegerOverflow)),
            #[cfg(feature = "rational-numbers")]
            (Value::Rational(il), Value::Integer(ir)) =>
                il.checked_add(ir)
                    .map(Value::Rational)
                    .unwrap_or(Value::Error(Error::IntegerOverflow)),

            _ => Value::Error(Error::InvalidOperation),
        }
    }
}

impl ops::Sub for Value {
    type Output = Value;

    fn sub(self, other: Value) -> Value {
        match (self, other) {
            (err @ Value::Error(_), _) | (_, err @ Value::Error(_)) => err,
            (Value::Integer(il), Value::Integer(ir)) =>
                il.checked_sub(ir)
                    .map(Value::Integer)
                    .unwrap_or(Value::Error(Error::IntegerUnderflow)),

            #[cfg(feature = "rational-numbers")]
            (Value::Rational(il), Value::Rational(ir)) =>
                il.checked_sub(ir)
                    .map(Value::Rational)
                    .unwrap_or(Value::Error(Error::IntegerUnderflow)),

            _ => Value::Error(Error::InvalidOperation),
        }
    }
}


impl ops::Mul for Value {
    type Output = Value;

    fn mul(self, other: Value) -> Value {
        match (self, other) {
            (err @ Value::Error(_), _) | (_, err @ Value::Error(_)) => err,
            (Value::Integer(il), Value::Integer(ir)) =>
                il.checked_mul(ir)
                    .map(Value::Integer)
                    .unwrap_or(Value::Error(Error::IntegerOverflow)),

            #[cfg(feature = "rational-numbers")]
            (Value::Rational(il), Value::Rational(ir)) =>
                il.checked_mul(ir)
                    .map(Value::Rational)
                    .unwrap_or(Value::Error(Error::IntegerOverflow)),

            _ => Value::Error(Error::InvalidOperation),
        }
    }
}

impl ops::Div for Value {
    type Output = Value;

    fn div(self, other: Value) -> Value {
        match (self, other) {
            (err @ Value::Error(_), _) | (_, err @ Value::Error(_)) => err,
            (Value::Integer(_), Value::Integer(0)) =>  Value::Error(Error::DivByZero),
            (Value::Integer(il), Value::Integer(ir)) =>
                il.checked_div(ir)
                    .map(Value::Integer)
                    .unwrap_or(Value::Error(Error::ArithmeticError)),

            #[cfg(feature = "rational-numbers")]
            (Value::Rational(il), Value::Rational(ir)) =>
                il.checked_div(ir)
                    .map(Value::Rational)
                    .unwrap_or(Value::Error(Error::ArithmeticError)),

            _ => Value::Error(Error::InvalidOperation),
        }
    }
}


#[cfg(test)]
mod tests {
    use super::Value::{Integer, Identifier};
    use super::*;

    mod partial_eq {
        // use super::*;

        // TODO: Get this working
        // #[test]
        // fn partial_eq_works_on_builtin_pointers() {
        //     fn op_const_0(_: &mut Backend, _: List<Value>) -> Value {
        //         Value::Integer(0)
        //     }

        //     fn op_const_1(_: &mut Backend, _: List<Value>) -> Value {
        //         Value::Integer(1)
        //     }

        //     let f1 = Value::BuiltinFunction(&'static op_const_0);
        //     let f2 = Value::BuiltinFunction(&'static op_const_1);

        //     assert_eq!(f1, f1);
        //     assert_ne!(f1, f2);
        // }
    }

    mod neg {
        use super::*;

        simple_test!(
            neg_error_carry,
            || -Value::Error(Error::UnboundVariable),
            Value::Error(Error::UnboundVariable)
        );

        simple_test!(neg_integer_pos, || -Integer(9), Integer(-9));
        simple_test!(neg_integer_neg, || -Integer(-9), Integer(9));
        simple_test!(neg_identifier, || {
            -Identifier("asd".into())
        }, Value::Error(Error::InvalidOperation));
    }


    mod add {
        use super::*;

        simple_test!(add_integer, || Integer(9) + Integer(1), Integer(10));
        simple_test!(
            add_integer_overflow,
            || Integer(super::super::Integer::max_value()) + Integer(1),
            Value::Error(Error::IntegerOverflow)
        );
        simple_test!(
            add_error_carry,
            || Value::Error(Error::UnboundVariable) + Integer(1),
            Value::Error(Error::UnboundVariable)
        );
    }

    mod sub {
        use super::*;

        simple_test!(sub_integer, || Integer(9) - Integer(1), Integer(8));
        simple_test!(
            sub_integer_underflow,
            || Integer(super::super::Integer::min_value()) - Integer(1),
            Value::Error(Error::IntegerUnderflow)
        );
        simple_test!(
            sub_error_carry,
            || Value::Error(Error::UnboundVariable) - Integer(1),
            Value::Error(Error::UnboundVariable)
        );
    }

    mod mul {
        use super::*;

        simple_test!(mul_integer, || Integer(9) * Integer(2), Integer(18));
        simple_test!(
            mul_error_carry,
            || Value::Error(Error::UnboundVariable) * Integer(1),
            Value::Error(Error::UnboundVariable)
        );
    }

    mod div {
        use super::*;

        // simple_test!(div_integer_inexact, || Integer(9) / Integer(2), Real(9/2));
        simple_test!(div_integer_exact, || Integer(9) / Integer(3), Integer(3));
        simple_test!(div_integer_0, || Integer(9) / Integer(0), Value::Error(Error::DivByZero));

        simple_test!(
            div_error_carry,
            || Value::Error(Error::UnboundVariable) * Integer(1),
            Value::Error(Error::UnboundVariable)
        );
    }

    mod from_ast {
        use parse::Ast;
        use super::*;

        mod integer {
            use super::*;

            simple_test!(valid_int, || {
                Value::from_ast(Ast::Integer("10"))
            }, Ok(Some(Value::Integer(10))));

            simple_test!(valid_neg_int, || {
                Value::from_ast(Ast::Integer("-10"))
            }, Ok(Some(Value::Integer(-10))));

            simple_test!(overflow_int, || {
                Value::from_ast(Ast::Integer("99999999999999999999999999999"))
            }, Err("Integer over capacity".into()));
        }

        mod identifier {
            use super::*;

            simple_test!(valid_ident, || {
                Value::from_ast(Ast::Identifier("test-ident"))
            }, Ok(Some(Value::Identifier("test-ident".into()))));
        }

        mod symbol {
            use super::*;

            simple_test!(valid_symbol, || {
                Value::from_ast(Ast::Symbol("symbol"))
            }, Ok(Some(Value::Symbol("symbol".into()))));
        }

        mod boolean {
            use super::*;

            simple_test!(valid_bool_t, || {
                Value::from_ast(Ast::Boolean("#t"))
            }, Ok(Some(Value::Boolean(true))));

            simple_test!(valid_bool_f, || {
                Value::from_ast(Ast::Boolean("#f"))
            }, Ok(Some(Value::Boolean(false))));
        }

        mod sexp {
            use super::*;

            simple_test!(valid_sexp, || {
                Value::from_ast(Ast::Sexp(vec![]))
            }, Ok(Some(Value::Sexp(vec![]))));

            simple_test!(valid_sexp_child, || {
                Value::from_ast(Ast::Sexp(vec![
                    Ast::Integer("1")
                ]))
            }, Ok(Some(Value::Sexp(vec![
                Value::Integer(1)
            ]))));

            #[test]
            fn fmt_sexp() {
                use alloc::string::String;
                use super::Value;
                assert_eq!(
                    format!("{}", Value::Sexp(vec![
                        Value::Identifier("/".into()),
                        Value::Integer(1),
                        Value::Integer(2),
                    ])),
                    String::from("(/ 1 2)")
                );
            }
        }


        mod qexp {
            use super::*;

            simple_test!(valid_qexp, || {
                Value::from_ast(Ast::Qexp(vec![]))
            }, Ok(Some(Value::Qexp(vec![]))));

            simple_test!(valid_qexp_child, || {
                Value::from_ast(Ast::Qexp(vec![
                    Ast::Integer("1")
                ]))
            }, Ok(Some(Value::Qexp(vec![
                Value::Integer(1)
            ]))));
        }
    }

    mod builtin_fn {
        use super::*;


        #[test]
        fn compares_correctly() {
            let fn_val = Value::BuiltinFunction(&(::evaluate::op_cdr as BuiltinFunction));
            assert_eq!(fn_val, fn_val);
        }
    }
}
