use super::*;
use core::ops::*;

pub trait Integer:
    Sized
    + Copy
    + Not<Output = Self>
    + BitAnd<Output = Self>
    + BitOr<Output = Self>
    + BitXor<Output = Self>
    + Add<Output = Self>
    + Sub<Output = Self>
    + Mul<Output = Self>
    + Div<Output = Self>
    + Rem<Output = Self>
    + Shl<Output = Self>
    + Shr<Output = Self>
    + BitAndAssign
    + BitOrAssign
    + BitXorAssign
    + AddAssign
    + SubAssign
    + MulAssign
    + DivAssign
    + RemAssign
    + ShlAssign
    + ShrAssign
    + CheckedAdd<Output = Self>
    + CheckedSub<Output = Self>
    + CheckedMul<Output = Self>
    + CheckedDiv<Output = Self>
    + CheckedRem<Output = Self>
    + CheckedNeg<Output = Self>
    + CheckedShl<Output = Self>
    + CheckedShr<Output = Self>
    + CheckedPow<Output = Self> {

}

macro_rules! integer_impl {
    ( $($t:ty)* ) => ($(
        impl Integer for $t {}
    )*)
}

integer_impl! { usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 }