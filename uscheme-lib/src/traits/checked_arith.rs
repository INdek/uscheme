/// This exists because num-traits Checked{Add, Sub, Mul, Div}
/// Does not allow for different out types
///
/// This is tracked in https://github.com/rust-num/num-traits/issues/47
/// but does not look like its going to be fixed

pub trait CheckedAdd<Rhs=Self> {
    type Output;

    #[must_use]
    fn checked_add(self, other: Rhs) -> Option<Self::Output>;
}

pub trait CheckedSub<Rhs=Self> {
    type Output;

    #[must_use]
    fn checked_sub(self, other: Rhs) -> Option<Self::Output>;
}

pub trait CheckedMul<Rhs=Self> {
    type Output;

    #[must_use]
    fn checked_mul(self, other: Rhs) -> Option<Self::Output>;
}

pub trait CheckedDiv<Rhs=Self> {
    type Output;

    #[must_use]
    fn checked_div(self, other: Rhs) -> Option<Self::Output>;
}

pub trait CheckedRem<Rhs=Self> {
    type Output;

    #[must_use]
    fn checked_rem(self, other: Rhs) -> Option<Self::Output>;
}

pub trait CheckedNeg {
    type Output;

    #[must_use]
    fn checked_neg(self) -> Option<Self::Output>;
}

// TODO: We might want to impl Rhs for these as well
pub trait CheckedShl {
    type Output;

    #[must_use]
    fn checked_shl(self, other: u32) -> Option<Self::Output>;
}

pub trait CheckedShr {
    type Output;

    #[must_use]
    fn checked_shr(self, other: u32) -> Option<Self::Output>;
}

pub trait CheckedPow {
    type Output;

    #[must_use]
    fn checked_pow(self, other: u32) -> Option<Self::Output>;
}


/// The reason why we need the intermediary function is that in a trait
/// you cannot call a inherent method with the same name.
///
/// See the footnote: https://doc.rust-lang.org/reference/expressions/method-call-expr.html
///
/// To get around this, we define a function, call that function
/// in the trait, and that function calls the inherent method
macro_rules! checked_impl_binop {
    ($trait: ident, $fname: ident, $tfn: ident, $t: ty) => {
        #[inline]
        fn $tfn(a: $t, b: $t) -> Option<$t> {
            a.$fname(b)
        }

        impl $trait for $t {
            type Output = $t;

            fn $fname(self, other: $t) -> Option<Self::Output> {
                $tfn(self, other)
            }
        }
    }
}

/// Methods like Integer::checked_pow/checked_shr
/// require u32 on the second argument
macro_rules! checked_impl_binop_u32 {
    ($trait: ident, $fname: ident, $tfn: ident, $t: ty) => {
        #[inline]
        fn $tfn(a: $t, b: u32) -> Option<$t> {
            a.$fname(b)
        }

        impl $trait for $t {
            type Output = $t;

            fn $fname(self, other: u32) -> Option<Self::Output> {
                $tfn(self, other)
            }
        }
    }
}

macro_rules! checked_impl_unop {
    ($trait: ident, $fname: ident, $tfn: ident, $t: ty) => {
        #[inline]
        fn $tfn(a: $t) -> Option<$t> {
            a.$fname()
        }

        impl $trait for $t {
            type Output = $t;

            fn $fname(self) -> Option<Self::Output> {
                $tfn(self)
            }
        }
    }
}


// TODO: Make a generic macro, that does all of this, when we have a way
// to concatenate identifiers

checked_impl_binop!(CheckedAdd, checked_add, __checked_add_u8, u8);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_u16, u16);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_u32, u32);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_u64, u64);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_u128, u128);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_usize, usize);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_i8, i8);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_i16, i16);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_i32, i32);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_i64, i64);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_i128, i128);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_isize, isize);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_f32, f32);
checked_impl_binop!(CheckedAdd, checked_add, __checked_add_f64, f64);

checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_u8, u8);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_u16, u16);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_u32, u32);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_u64, u64);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_u128, u128);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_usize, usize);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_i8, i8);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_i16, i16);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_i32, i32);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_i64, i64);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_i128, i128);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_isize, isize);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_f32, f32);
checked_impl_binop!(CheckedSub, checked_sub, __checked_sub_f64, f64);

checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_u8, u8);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_u16, u16);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_u32, u32);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_u64, u64);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_u128, u128);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_usize, usize);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_i8, i8);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_i16, i16);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_i32, i32);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_i64, i64);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_i128, i128);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_isize, isize);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_f32, f32);
checked_impl_binop!(CheckedMul, checked_mul, __checked_mul_f64, f64);

checked_impl_binop!(CheckedDiv, checked_div, __checked_div_u8, u8);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_u16, u16);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_u32, u32);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_u64, u64);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_u128, u128);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_usize, usize);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_i8, i8);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_i16, i16);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_i32, i32);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_i64, i64);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_i128, i128);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_isize, isize);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_f32, f32);
checked_impl_binop!(CheckedDiv, checked_div, __checked_div_f64, f64);

checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_u8, u8);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_u16, u16);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_u32, u32);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_u64, u64);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_u128, u128);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_usize, usize);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_i8, i8);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_i16, i16);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_i32, i32);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_i64, i64);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_i128, i128);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_isize, isize);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_f32, f32);
checked_impl_binop!(CheckedRem, checked_rem, __checked_rem_f64, f64);

checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_u8, u8);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_u16, u16);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_u32, u32);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_u64, u64);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_u128, u128);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_usize, usize);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_i8, i8);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_i16, i16);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_i32, i32);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_i64, i64);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_i128, i128);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_isize, isize);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_f32, f32);
checked_impl_unop!(CheckedNeg, checked_neg, __checked_neg_f64, f64);

checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_u8, u8);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_u16, u16);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_u32, u32);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_u64, u64);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_u128, u128);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_usize, usize);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_i8, i8);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_i16, i16);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_i32, i32);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_i64, i64);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_i128, i128);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_isize, isize);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_f32, f32);
checked_impl_binop_u32!(CheckedShl, checked_shl, __checked_shl_f64, f64);

checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_u8, u8);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_u16, u16);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_u32, u32);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_u64, u64);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_u128, u128);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_usize, usize);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_i8, i8);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_i16, i16);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_i32, i32);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_i64, i64);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_i128, i128);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_isize, isize);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_f32, f32);
checked_impl_binop_u32!(CheckedShr, checked_shr, __checked_shr_f64, f64);

checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_u8, u8);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_u16, u16);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_u32, u32);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_u64, u64);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_u128, u128);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_usize, usize);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_i8, i8);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_i16, i16);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_i32, i32);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_i64, i64);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_i128, i128);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_isize, isize);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_f32, f32);
checked_impl_binop_u32!(CheckedPow, checked_pow, __checked_pow_f64, f64);


// checked_impl_binop!     { CheckedAdd, checked_add, usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 f32 f64 }
// checked_impl_binop!     { CheckedSub, checked_sub, usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 f32 f64 }
// checked_impl_binop!     { CheckedMul, checked_mul, usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 f32 f64 }
// checked_impl_binop!     { CheckedDiv, checked_div, usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 f32 f64 }
// checked_impl_binop!     { CheckedRem, checked_rem, usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 f32 f64 }
// checked_impl_unop!      { CheckedNeg, checked_neg, usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 f32 f64 }
// checked_impl_binop_u32! { CheckedShl, checked_shl, usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 f32 f64 }
// checked_impl_binop_u32! { CheckedShr, checked_shr, usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 f32 f64 }
// checked_impl_binop_u32! { CheckedPow, checked_pow, usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128 f32 f64 }