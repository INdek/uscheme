macro_rules! list {
    ( $( $val:expr ),* ) => [{
        #[allow(unused_mut)]
        let mut list = ::list::List::new();
        $(
            list.push($val);
        )*
        list
    }]
}

// This won't be used most of the time, but its usefull to have
// on demand for debugging
#[allow(unused_macros)]
macro_rules! output {
    ( $( $val:expr ),* ) => {
        #[cfg(all(not(fuzzing), not(feature = "no_std")))]
        println!($($val),*);
    }
}


#[cfg(test)]
macro_rules! parse_test {
    (simple: $fn: ident, $parsefn: expr, $input: expr) => {
        #[test]
        #[allow(unused_imports)]
        fn $fn() {
            use ::*;
            match $parsefn(::nom::types::CompleteByteSlice($input)) {
                Ok((::nom::types::CompleteByteSlice(b""), _)) => {
                    return;
                }
                _ => assert!(false)
            }
        }
    };
    ($fn: ident, $parsefn: expr, $input: expr, $output: expr) => {
        #[test]
        #[allow(unused_imports)]
        fn $fn() {
            use ::*;
            assert_eq!(
                $parsefn(::nom::types::CompleteByteSlice($input)),
                Ok((::nom::types::CompleteByteSlice(b""), $output))
            );
        }
    };
}


#[cfg(test)]
macro_rules! equivalence_test {
    (ignore: $fn: ident, $output: expr, $( $input: expr ),* ) => {
        #[test]
        #[ignore]
        fn $fn() {}
    };
    (eval: $e: expr) => {{
        use ::*;

        let mut backend = backend::Backend::new();
        let e = $e;
        let input = parse::parse(e.as_bytes()).unwrap();
        backend.eval_ast_list(input.1).unwrap().unwrap()
    }};
    (body: $lhs: expr, $rhs: expr) => {{
        assert_eq!(
            equivalence_test!(eval: $lhs),
            equivalence_test!(eval: $rhs),
        )
    }};
    ($fn: ident, $lhs: expr, $rhs: expr ) => {
        #[test]
        #[allow(unused_imports, unused_variables)]
        fn $fn() {
            equivalence_test!(body: $lhs, $rhs)
        }
    };
}


#[cfg(test)]
macro_rules! eval_test {
    (ignore: $fn: ident, $output: expr, $( $input: expr ),* ) => {
        #[test]
        #[ignore]
        fn $fn() {}
    };
    (body: $output: expr, $( $input: expr ),*) => {{
        use ::*;
        use core::convert::TryFrom;

        let mut backend = backend::Backend::new();

        let input_exprs = list![
            $( parse::parse(&$input[..]).unwrap() ),*
        ].into_iter()
            .map(|ast| ast.1)
            .flatten()
            .collect();

        let val = backend.eval_ast_list(input_exprs).unwrap().unwrap();

        assert_eq!($output, val);
    }};
    ($fn: ident, $output: expr, $( $input: expr ),* ) => {
        #[test]
        #[allow(unused_imports, unused_variables)]
        fn $fn() {
            eval_test!(body: $output, $($input),*)
        }
    };
}


#[cfg(test)]
macro_rules! simple_test {
    ($fn: ident, $input: expr, $output: expr) => {
        #[test]
        #[allow(unused_imports)]
        fn $fn() {
            use ::*;

            assert_eq!($input(), $output);
        }
    };
}
