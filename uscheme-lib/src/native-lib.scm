(define abs (lambda (num)
    (max (- num) num)))

(define zero? (lambda (num)
    (= num 0)))

(define positive? (lambda (num)
    (> num 0)))

(define negative? (lambda (num)
    (< num 0)))