use environment::Environment;
use value::*;
use alloc::string::String;
use parse::{Ast, parse};
use list::List;

const NATIVE_LIB: &'static str = include_str!("./native-lib.scm");

#[derive(Debug)]
pub struct Backend {
    pub env: Environment,
}

impl Backend {
    pub fn new() -> Self {
        Default::default()
    }


    fn call_fn(
        &mut self,
        arg_names: List<Value>,
        args: List<Value>,
        body: List<Value>
    ) -> Value {
        if arg_names.len() != args.len() {
            return Value::Error(Error::ArityError);
        }

        self.env.push_scope();

        let args: List<Value> = args.into_iter()
            .map(|arg| self.eval(arg))
            .collect();

        // All this does is register arguments in the most local scope
        let env_result: Result<(), Value> = arg_names.into_iter()
            .fold(Ok(list![]), |acc, an| match (an, acc) {
                (Value::Identifier(ident), Ok(mut list)) => {
                    list.push(ident);
                    Ok(list)
                },
                _ => Err(Value::Error(Error::InvalidOperation)),
            })
            .map(|names| {
                names.into_iter()
                    .zip(args.into_iter())
                    .map(|(name, arg)| self.env.insert(name, arg))
                    .collect()
            });

        if let Err(eval) = env_result {
            self.env.pop_scope();
            return eval;
        }

        let ret_val = body.into_iter()
            .fold(Value::Sexp(list![]), |_, expr| {
                self.eval(expr)
            });

        self.env.pop_scope();

        ret_val
    }



    fn eval_sexp(&mut self, mut args: List<Value>) -> Value {
        if args.len() == 0 {
            return Value::Sexp(List::new());
        }

        // TODO: test qexp on first element of sexp
        let first = args.remove(0);
        match first {
            Value::Function { body, args: arg_names } => self.call_fn(arg_names, args, body),
            Value::BuiltinFunction(op_fn) => op_fn(self, args),

            Value::Sexp(ref subargs) if subargs.len() == 0 =>
                Value::Error(Error::InvalidOperation),

            s @ Value::Sexp(_) => {
                args.insert(0, self.eval(s));
                self.eval_sexp(args)
            },
            Value::Identifier(name) => {
                let new_var = self.env.lookup(&name).clone();
                // output!("Looked up {}: Found: {:?}", name, new_var);
                args.insert(0, new_var);
                self.eval_sexp(args)
            },
            _ => Value::Error(Error::InvalidOperation),
        }
    }

    pub fn eval(&mut self, value: Value) -> Value {
        // output!("Eval: {:?}", value);
        match value {
            Value::Sexp(args) => self.eval_sexp(args),
            Value::Identifier(sname) => self.env.lookup(&sname).clone(),
            _ => value,
        }
    }

    pub fn eval_ast(&mut self, ast: Ast) -> Result<Option<Value>, String> {
        Value::from_ast(ast)
            .map(|opt_val| opt_val.map(|val| self.eval(val)))
    }

    pub fn eval_ast_list(&mut self, ast_list: List<Ast>) -> Result<Option<Value>, String> {
        ast_list.into_iter()
            .fold(Ok(None), |acc, ast| match acc {
                Err(_) => acc,
                _ => self.eval_ast(ast),
            })
    }

    // TODO: Test evaluate many lines, currently only evaluates first
    pub fn eval_string<S: AsRef<str>>(&mut self, source: S) -> Result<Option<Value>, String> {
        let source = source.as_ref();
        match parse(source.as_bytes()) {
            Ok(ast) => Ok(self.eval_ast_list(ast.1)?),
            Err(err) => Err(format!("Parse error: {:?}", err)),
        }
    }
}

impl Default for Backend {
    fn default() -> Backend {
        use evaluate::*;

        let mut backend = Backend {
            env: Environment::default()
        };

        let map: &[(&str, BuiltinFunction)] = &[
            ("+", op_sum),
            ("*", op_mul),
            ("-", op_sub),
            ("/", op_div),
            ("eval", op_eval),
            ("car", op_car),
            ("cdr", op_cdr),
            ("list", op_list),
            ("define", op_define),
            ("lambda", op_lambda),
            ("quote", op_quote),
            (">", op_gt),
            (">=", op_gte),
            ("<", op_lt),
            ("<=", op_lte),
            ("=", op_eq),
            ("if", op_if),
            ("print", op_print),

            #[cfg(not(no_std))]
            ("load", op_load),

            ("equal?", op_equal),

            ("odd?", op_odd),
            ("even?", op_even),
            ("max", op_max),
            ("min", op_min),
            ("quotient", op_quotient),
        ];

        for (name, f) in map {
            backend.env.insert((*name).into(), Value::BuiltinFunction(f));
        }

        let native_lib = ::parse::parse(NATIVE_LIB.as_bytes())
            .expect("Failed to parse native library").1;

        backend.eval_ast_list(native_lib)
            .expect("Failed to compile native library")
            .expect("Failed to compile native library");

        backend
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_multi_eval() {
        let mut backend = Backend::default();
        let res = backend.eval_string(r#"
            (+ 1 2)
            (+ 3 4)
        "#);

        assert_eq!(res, Ok(Some(Value::Integer(7))));
    }
}